const MergeSort = (arrayToSort = []) => {
    const len = arrayToSort.length
    let arr1, arr2, returnArray
    if(len > 2) {
        const middleIndex = ( len % 2 === 0 ? len/2 : ((len - 1) / 2) + 1)
        arr1 = MergeSort(arrayToSort.splice(0, middleIndex))
        arr2 = MergeSort(arrayToSort.splice(0, arrayToSort.length))
        returnArray = []

        while(arr1.length > 0) {
            if(arr1[0] > arr2[0]) {
                returnArray.push(...arr2.splice(0, 1))
            } else {
                returnArray.push(...arr1.splice(0, 1))
            }
        }
        if(arr2.length > 0) {
            returnArray.push(...arr2)
        }

    } else {
        if(len === 1) {
            returnArray = arrayToSort
        } else {
            const zeroIndex = arrayToSort[0],
                firstIndex = arrayToSort[1]
            if(zeroIndex > firstIndex) {
                returnArray = [firstIndex, zeroIndex]
            } else {
                returnArray = [zeroIndex, firstIndex]
            }
        }
    }
    return returnArray
}

console.log(MergeSort([ 5,7,16,24,85,60,1,9,8,7,9, ]))
console.log(MergeSort([ 38,27,43,3,9,82,10 ]))
console.log(MergeSort([12,11,13,5,6,7]))
console.log(MergeSort( [500,489,5,10,12,89,70,501,309,269,270,150,120] ))
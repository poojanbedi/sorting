function selectionSort(arr) {
  let minValueIndex;
  let tempVal;
  for(let index = 0, len = arr.length; index < len; index += 1) {
    minValueIndex = index;
    for(var indexJ = index + 1; indexJ < len; indexJ += 1) {
      if(arr[indexJ] < arr[minValueIndex]) {
        minValueIndex = indexJ;
      }
    }
    tempVal = arr[index];
    arr[index] = arr[minValueIndex];
    arr[minValueIndex] = tempVal;
  }
  return arr;
}

console.log(selectionSort([ 4,2,4,5,7,8,9,0,10,6,100,44,35,65,76,12 ]));
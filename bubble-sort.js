function bubbleSort(arr) {
  let arrLength = arr.length - 1;
  let isArraySorted = false;
  while(!isArraySorted) { // Run untill array is sorted
    isArraySorted = true;
    for(let index = 0; index < arrLength; index += 1) {  
      if(arr[index] > arr[index + 1]) {
        let _arrVal = arr[index];
        arr[index] = arr[index + 1];
        arr[index + 1] = _arrVal;
        isArraySorted = false;
      }
    }
    arrLength -= 1;
  }
  return arr;
}

console.log(bubbleSort([ 4,2,4,5,7,8,9,0,10,6,100,44,35,65,76,12 ]));
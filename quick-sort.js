const QuickSort = (arrayToSort) => {
    const pivot = arrayToSort.pop() // Lets assume pivot is the last element.
    let lessThanArray = [], greaterThanArray = []

    while(arrayToSort.length !== 0) {
        const currentItem = arrayToSort.splice(0, 1)[0]
        if(currentItem <= pivot) {
            lessThanArray.push(currentItem)
        } else {
            greaterThanArray.push(currentItem)
        }
    }

    if(lessThanArray.length > 1 || greaterThanArray.length > 1) {
        lessThanArray = lessThanArray.length > 0 ? QuickSort(lessThanArray) : lessThanArray
        greaterThanArray = greaterThanArray.length > 0 ? QuickSort(greaterThanArray) : greaterThanArray
    }
    return [...lessThanArray, pivot, ...greaterThanArray]
}

console.log(QuickSort([ 5,7,16,24,85,60,1,9,8,7,9, ]))
console.log(QuickSort([ 38,27,43,3,9,82,10 ]))
console.log(QuickSort([12,11,13,5,6,7]))
console.log(QuickSort( [500,489,5,10,12,89,70,501,309,269,270,150,120] ))